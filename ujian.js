// soal1
function changeWorld(selectedText,changeText,text){
  //menggunakan fungtion seperti ini
  let change = text.replace(selectedText,changeText)
  //menggunakan metode replace yaitu untuk string replace sama seperti push&pop,lalu kita masukan selectedText dan changeText jadikan 1 variabel change
  return change
}
//input
const kalimat1 = "Andini mencintai kamu selamanya"
const kalimat2 = "Gunung bromo tak akan mampu menggambarkan besarnya cintaku padamu"

//output
//andini M=membenci kamu selamanya
//gunung semeru tak akan mampu menggambarkan besarnya cintaku padamu

console.log(changeWorld('mencintai','membenci',kalimat1))
console.log(changeWorld('bromo','semeru',kalimat2))

//soal 2
const checkTypeNumber =(givenNumber)=>{//menggunakan metode arrow fungtion
if (givenNumber === undefined){//jika kondisi givenNumber samadengan undifined atau not difined akan tercetak(Error:Bro where is the parameter) tidak ada angka ganjil atau genap
  return "Error:Bro where is the parameter";//jika null tidak ada isi nya kalau undifined masih ada devinisi nya, karena hoisting isi undifined
}else if (typeof givenNumber !== "number"){//jika kondisi givenNumber tidaksamadengan number akan tercetak (Error: invalid data type)
  return "Error:invalid data type";
}else return givenNumber % 2 === 0 ?"Genap" : "Ganjil";//jika kondisi givenNumber modulus 2 samadengan 0 akan tercetak (genap)
};

console.log(checkTypeNumber(10));//OUTPUT yang keluar =>"GENAP"
console.log(checkTypeNumber(3));//OUTPUT yang keluar =>"GANJIL"
console.log(checkTypeNumber("3"));//OUTPUT yang keluar =>"Error: Invalid data type"
console.log(checkTypeNumber({}));//OUTPUT yang  keluar =>"Error: Invalid data type"
console.log(checkTypeNumber([]));//OUTPUT yang  keluar =>"Error: Invalid data type"
console.log(checkTypeNumber());
//OUTPUT yang keluar => "Error : Bro where is the parameter?"

// //soal 3
function checkEmail(email){//kita membuat funtion checkEmail
  //check input
    if (email === undefined){//apabila email samadengan undifined akan tercetak undifined (undifined masih ada devinisi)
      return "Error: There is not email to check";
    }
  if (typeof email !== "string"){//jika menggunakan typeof jenis yang akan di ambil dan apabila email sama dengan string atau pada saat menginput ada angka
      return "Error: email should be a string";
    }
  if (!/[@]/.test(email)){//nilai test ini akan mengembalikan nilai true apabila character itu cocok apabila tidak cocok akan jdi false (email adalah inputan parametter yg di atas)
      return "Error: email should containt '@' character"; //syarat data tersebut
  }
//process
  if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email)){//regex ini menyulitkan hacker agar tidak bisa di bobol web browser kita
    return "VALID";
  }else {
    return "INVALID"
  }//apabila mencangkup @ string dan undivined makan akan tercetak Error
}

console.log(checkEmail('apranata@binar.co.id'));//output yang keluar valid
console.log(checkEmail('apranata@binar.com'));//output yang keluar valid
console.log(checkEmail('apranata@binar'));//output yang keluar invalid
console.log(checkEmail('apranata'));//output yang keluar Error: karena tidak termasuk kode uniq regex '@'
console.log(checkTypeNumber(checkEmail(3322)));//Error kenapa? karena tidak ada fungtion CekTypeNumber
console.log(checkEmail());//Error karena undivined tidak ada isi nya there is not email to check

//soal 4

function isValidPassword(password) {//kita membuat fungtion isValidPassword 
  //check input
  if (password === undefined) {//apabila dia sama dengan undifined akan false
    console.log("Error: there is no password");
    return false;
  }
  if (typeof password !== "string") {//apablia dia sama dengan string akan false
    console.log("Error: A password should be in string type");
    return false;
  }
  const isValid = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[\w\W]{8,}$/.test(password);//mengecek validasi(apabila kita tambahkan @ makan akan ttp invalid)
  return isValid;
}
console.log(isValidPassword("Meong@2021"));//kenapa true karena teridentifikasi 1 huruf besar, lebih dari 8 character 
console.log(isValidPassword("@eong"));//kenapa false karena teridentifikasi tidak ada 1 huruf besar
console.log(isValidPassword("Meong2"));//kenapa false karena teridentifikasi string dan tidak ada regex '@'
console.log(isValidPassword(0));//kenaoa false karena undivined
console.log(isValidPassword());//kenapa false karena undivined

//soal 5

function getSplitName(personName){//membuat fungtion dan 1 parameter
  //check data type before start the process

  if (typeof personName !== "string")return "Error: Invalid data type";
  
  //start splitting process
  const splittedName= personName.split(" ");//variable bisa menyimpan parameter beserta metode untung string
    if (splittedName.length > 3){
    return "Error: This funtion is only for 3 characters name";

  }else if (splittedName.length === 3){
    return{
      firstName: splittedName[0],
      middleName:splittedName[1],
      lastName:splittedName[2],
    };
  } else if (splittedName.length === 2) {
    return{
      firstName:splittedName[0],
      middleName:null,
      lastName:splittedName[1],
    };
  }else if (splittedName.length === 1){
    return{firstName: splittedName[0], middleName: null, lastName:null,};
  }
}

console.log(getSplitName("Adi Daniela Pratama"));//
console.log(getSplitName("Dwi Kuncoro"));
console.log(getSplitName("Aurora"));
console.log(getSplitName(0));


//soal 6

function getAngkaTerbesarKedua(dataNumbers){
  //check the input
  if (dataNumbers === undefined) return "Error: function need an array parameter";
  if(!Array.isArray(dataNumbers))return "Error: Invalid data type";
  if(dataNumbers.length < 2)return "Error: Array length should be 2 or longer";

  //start the process
  let first = Number.MIN_SAFE_INTEGER;//MIN_SAFE_INTEGER mewakili bilangan bulat aman minimum dalam JavaScript
  let second = Number.MIN_SAFE_INTEGER;
  let isContaintMinVal = false;
  dataNumbers.forEach((number) => {//Perulangan foreach biasanya digunakan untuk mencetak item di dalam array. Perulangan ini termasuk dalam perulangan counted loop, karena jumlah perulangannya akan dituentukan oleh panjang dari array
    if (number > first){
      first = number;
    }else if (number !== first && number > second) {
      second = number;
    }else if (number === Number.MIN_SAFE_INTEGER) {
      isContaintMinVal = true;
    }
  });

  if (second !== Number.MIN_SAFE_INTEGER || isContaintMinVal ) return second;
  else return "there is not second largest number";
}

const dataNumbers = [9,4,7,7,4,3,2,2,8];
console.log(getAngkaTerbesarKedua(dataNumbers));
console.log(getAngkaTerbesarKedua(0));
console.log(getAngkaTerbesarKedua());

// soal 7

const dataPenjualanPakAldi = [
  {
    namaProduct:"Sepatu Futsal Nike Vapor Academy 8",
    hargaSatuan: 760000,
    kategori:"Sepatu Sport",
    totalTerjual:90,
  },
  {
    namaProduct:"Sepatu Warrior Tristan Black Brown High",
    hargaSatuan: 960000,
    kategori:"Sepatu Snecker",
    totalTerjual:37,
  },
  {
    namaProduct:"Sepatu Warrior Tristan Maroon High",
    hargaSatuan: 360000,
    kategori:"Sepatu Snecker",
    totalTerjual:90,
  },
  {
    namaProduct:"Sepatu Warrior Rainbow Tosca Corduroy",
    hargaSatuan: 960000,
    kategori:"Sepatu Snecker",
    totalTerjual:90,
  },
];

function getTotalPenjual(dataPenjualan){
  //check data type
  if (!Array.isArray(dataPenjualan))
  return "Error: data penjualan harus berupa array";
  //start process
  const TotalPenjualan=dataPenjualan.reduce((total, data) =>{ //reduce adalah () merupakan method array di JavaScript yang mengeksekusi fungsi callback pada setiap elemen array, nilai hasil kalkulasi pada elemen sebelumnya digunakan untuk melakukan kalkulasi pada elemen berikutnya. Setelah menjalankan fungsi callback untuk semua elemen array, method ini menghasilkan nilai tunggal.
    return total+data.totalTerjual;
  }, 0);
  if (isNaN(TotalPenjualan)) return "Error: format data salah ! ";
  else return TotalPenjualan;
}

console.log(getTotalPenjual(dataPenjualanPakAldi));
//output 307 dari setiap value property 'Total terjual' yaitu 90+37+90+90

//soal 8
const dataPenjualanNovel = [
  {
    idProduk: "BOOK002421",
    namaProduk: "Pulang - Pergi",
    penulis: "Tere Liye",
    hargaBeli: 60000,
    hargaJual: 86000,
    totalTerjual: 150,
    sisaStok: 17,
  },
  {
    idProduk: "BOOK002351",
    namaProduk: "Selamat Tinggal",
    penulis: "Tere Liye",
    hargaBeli: 75000,
    hargaJual: 103000,
    totalTerjual: 171,
    sisaStok: 20,
  },
  {
    idProduk: "BOOK002941",
    namaProduk: "Garis Waktu",
    penulis: "Fiersa Besari",
    hargaBeli: 67000,
    hargaJual: 99000,
    totalTerjual: 213,
    sisaStok: 5,
  },
  {
    idProduk: "BOOK002942",
    namaProduk: "Laskar Pelangi",
    penulis: "Andrea Hirata",
    hargaBeli: 55000,
    hargaJual: 68000,
    totalTerjual: 20,
    sisaStok: 56,
  },
];
console.log(getInfoPenjualan(dataPenjualanNovel));

function getInfoPenjualan(dataPenjualan) {

  if (!Array.isArray(dataPenjualan)) {
    console.error("Invalid data type");
    return {};
  }

  let totalKeuntungan = 0;//kenapa milih let Karena kedua variabel tersebut dianggap berada dan juga di cakupan yang berbeda
  let totalModal = 0;
  let produkTerlaris = dataPenjualan[0];
  let persentaseKeuntungan = 0;
  let penulisBuku = {};
  let penulisTerlaris = "";

  dataPenjualan.forEach((data) => {
    modal = data.hargaBeli * (data.totalTerjual + data.sisaStok);
    keuntungan = data.hargaJual * data.totalTerjual - modal;
    totalModal += modal;
    totalKeuntungan += keuntungan;
    if (data.totalTerjual > produkTerlaris.totalTerjual) {
      produkTerlaris = data;
    }
    if (penulisBuku[data.penulis])
      penulisBuku[data.penulis] += data.totalTerjual;
    else penulisBuku[data.penulis] = data.totalTerjual;
  });
  console.log(penulisBuku);
  persentaseKeuntungan = (totalKeuntungan / totalModal) * 100;
  penulisTerlaris = Object.keys(penulisBuku).reduce((a, b) => {
    return penulisBuku[a] > penulisBuku[b] ? a : b;
  });

  const result = {
    totalKeuntungan: "Rp. " + totalKeuntungan.toLocaleString("id-ID"),
    totalModal: "Rp. " + totalModal.toLocaleString("id-ID"),
    persentaseKeuntungan: Math.round(persentaseKeuntungan) + "%",
    produkTerlaris: produkTerlaris.namaProduk,
    penulisTerlaris: penulisTerlaris,
  };
  return result;
}

